# node로 npm install 후 ng build --prod
FROM node:8.11.4-alpine as node

# package.json을 복사
COPY package.json ./

RUN npm set progress=false && npm config set depth 0 && npm cache clean --force

RUN npm i && mkdir /ng-app && cp -R ./node_modules ./ng-app

WORKDIR /ng-app

COPY . .

RUN $(npm bin)/ng build --prod


# nginx
FROM nginx:latest
COPY nginx.conf /etc/nginx/conf.d/

RUN rm -rf /usr/share/nginx/html/*

# COPY dist /usr/share/nginx/html

COPY --from=node /ng-app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
